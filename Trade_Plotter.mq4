//                     Trade_Plotter.mq4        
//
//                     by arts


#property indicator_chart_window

int  OT=-1;
int OT2 =-1;
//+------------------------------------------------------------------+

void init() 
{
  
 return(0);   
}

//+------------------------------------------------------------------+

void deinit() 
{
  
      int OBTotal=ObjectsTotal();

      for (int i=OBTotal; i>=0; i--) 
         {  
          if(StringSubstr(ObjectName(i),0, 2)=="# ")ObjectDelete(ObjectName(i));
         }
      
 return(0);    
}
  
//+------------------------------------------------------------------+


 int start() 
   {
   //Trade Analysis
     int cnt, total, total2;
     total=OrdersTotal();
     total2=OrdersHistoryTotal();
     color col, ccol, linecol;
     string PL="";  
   if(OT==total && OT2==total2)return(0);//no change in orders to plot
     
     //Plot Orders History 
     for(cnt=0;cnt<=total2;cnt++)
          {
           OrderSelect(cnt, SELECT_BY_POS, MODE_HISTORY);
           if(OrderSymbol()==Symbol() )  // check for symbol
               {
                if(OrderType()==OP_BUY)  { col=Green; ccol=PaleGreen; linecol=Blue; 
                                           PL=DoubleToStr((OrderClosePrice()-OrderOpenPrice())/Point/10,1)+" pips";}
                if(OrderType()==OP_SELL) { col=Red; ccol=Violet; linecol=Maroon; 
                                           PL=DoubleToStr((OrderOpenPrice()-OrderClosePrice())/Point/10,1)+" pips";}
                
                if(OrderType()==OP_BUY || OrderType()==OP_SELL)
                {
                ObjectCreate("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),OBJ_ARROW,0,OrderOpenTime(),OrderOpenPrice());
                ObjectSet("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),6, col);
                ObjectSet("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),14,1);
                
                ObjectCreate("# "+OrderTicket()+" : Close at "+TimeToStr(OrderCloseTime(),TIME_MINUTES),OBJ_ARROW,0,OrderCloseTime(),OrderClosePrice());
                ObjectSet("# "+OrderTicket()+" : Close at "+TimeToStr(OrderCloseTime(),TIME_MINUTES),6, ccol);
                ObjectSet("# "+OrderTicket()+" : Close at "+TimeToStr(OrderCloseTime(),TIME_MINUTES),14,3);
              
                ObjectCreate("# "+OrderTicket()+" : "+PL, OBJ_TREND, 0, OrderOpenTime(),OrderOpenPrice(),OrderCloseTime(),OrderClosePrice());
                ObjectSet("# "+OrderTicket()+" : "+PL,6,linecol);
                ObjectSet("# "+OrderTicket()+" : "+PL,7,2);
                ObjectSet("# "+OrderTicket()+" : "+PL,10,false);
                }
               }            
          }
     //Plot current orders
     for(cnt=0;cnt<=total;cnt++)
        {
         OrderSelect(cnt, SELECT_BY_POS, MODE_TRADES);
         if(OrderSymbol()==Symbol() )   // check for symbol
             {
              if(OrderType()==OP_BUY)   col=Green; 
              if(OrderType()==OP_SELL)  col=Red; 
              if(OrderType()==OP_BUY ||OrderType()==OP_SELL)
                {
                 ObjectCreate("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),OBJ_ARROW,0,OrderOpenTime(),OrderOpenPrice());
                 ObjectSet("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),6, col);
                 ObjectSet("# "+OrderTicket()+" : Open at "+TimeToStr(OrderOpenTime(),TIME_MINUTES),14,1);
                }
             }            
        }
         
     
    OT=total;
    OT2=total2;
    
    return(0);
   }

